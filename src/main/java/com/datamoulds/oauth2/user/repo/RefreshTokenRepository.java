package com.datamoulds.oauth2.user.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.datamoulds.oauth2.user.model.RefreshToken;

@Repository
public interface RefreshTokenRepository extends MongoRepository<RefreshToken,String> {

  RefreshToken findByTokenId(String tokenId);

  void deleteByTokenId(String tokenId);
}
