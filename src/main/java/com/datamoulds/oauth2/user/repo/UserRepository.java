package com.datamoulds.oauth2.user.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.datamoulds.oauth2.user.model.User;

public interface UserRepository extends MongoRepository<User, String> {

	User findByUserName(String userName);

}