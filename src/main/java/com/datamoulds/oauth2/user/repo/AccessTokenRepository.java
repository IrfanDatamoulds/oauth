package com.datamoulds.oauth2.user.repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.datamoulds.oauth2.user.model.AccessToken;

@Repository
public interface AccessTokenRepository extends MongoRepository<AccessToken,String>{

	  AccessToken findByTokenId(String tokenId);

	  AccessToken findByRefreshToken(String refreshToken);

	  AccessToken findByAuthenticationId(String authenticationId);

	  List<AccessToken> findByClientId(String clientId);

	  List<AccessToken> findByClientIdAndUserName(String clientId, String userName);

	  void deleteByRefreshToken(String refreshToken);

	  void deleteByTokenId(String tokenId);

	  List<AccessToken> findByUserNameAndClientId(String userName, String clientId);
}
