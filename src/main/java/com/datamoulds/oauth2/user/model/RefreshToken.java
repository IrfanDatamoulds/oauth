package com.datamoulds.oauth2.user.model;


import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "refresh_tokens")
public class RefreshToken {
	
	    private String tokenId;
	    private byte[] token;
	    private byte[] authentication;

	    public void RefreshToken(){}

	    @PersistenceConstructor
		public RefreshToken(String tokenId, byte[] token, byte[] authentication) {
			super();
			this.tokenId = tokenId;
			this.token = token;
			this.authentication = authentication;
		}

		public String getTokenId() {
			return tokenId;
		}

		public void setTokenId(String tokenId) {
			this.tokenId = tokenId;
		}

		public byte[] getToken() {
			return token;
		}

		public void setToken(byte[] token) {
			this.token = token;
		}

		public byte[] getAuthentication() {
			return authentication;
		}

		public void setAuthentication(byte[] authentication) {
			this.authentication = authentication;
		}
	    
	    
	    
    
}
