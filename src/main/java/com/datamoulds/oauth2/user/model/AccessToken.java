package com.datamoulds.oauth2.user.model;

import java.io.Serializable;

import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "access_tokens")
public class AccessToken implements Serializable {

	
	    private String tokenId;
	    private byte[] token;
	    private String authenticationId;
	    private String userName;
	    private String clientId;
	    private byte[] authentication;
	    private String refreshToken;
	   
	    public void AccesToken() {}
	    
	    @PersistenceConstructor
		public AccessToken(String tokenId, byte[] token, String authenticationId, String userName, String clientId,
				byte[] authentication, String refreshToken) {
			this.tokenId = tokenId;
			this.token = token;
			this.authenticationId = authenticationId;
			this.userName = userName;
			this.clientId = clientId;
			this.authentication = authentication;
			this.refreshToken = refreshToken;
		}

		public String getTokenId() {
			return tokenId;
		}

		public void setTokenId(String tokenId) {
			this.tokenId = tokenId;
		}

		public byte[] getToken() {
			return token;
		}

		public void setToken(byte[] token) {
			this.token = token;
		}

		public String getAuthenticationId() {
			return authenticationId;
		}

		public void setAuthenticationId(String authenticationId) {
			this.authenticationId = authenticationId;
		}
		
		

				public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

				public String getClientId() {
			return clientId;
		}

		public void setClientId(String clientId) {
			this.clientId = clientId;
		}

		public byte[] getAuthentication() {
			return authentication;
		}

		public void setAuthentication(byte[] authentication) {
			this.authentication = authentication;
		}

		public String getRefreshToken() {
			return refreshToken;
		}

		public void setRefreshToken(String refreshToken) {
			this.refreshToken = refreshToken;
		}
	    
	    

  
}
