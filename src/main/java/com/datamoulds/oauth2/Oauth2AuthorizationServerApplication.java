package com.datamoulds.oauth2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.approval.TokenStoreUserApprovalHandler;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.AuthenticationKeyGenerator;
import org.springframework.security.oauth2.provider.token.DefaultAuthenticationKeyGenerator;
import org.springframework.security.oauth2.provider.token.TokenStore;

import com.datamoulds.oauth2.user.repo.UserRepository;

@SpringBootApplication
public class Oauth2AuthorizationServerApplication {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	  private ClientDetailsService clientDetailsService;
	
	public static void main(String[] args) {
		SpringApplication.run(Oauth2AuthorizationServerApplication.class, args);
	}

	/*@Override
	public void run(String... args) throws Exception {
		if (this.userRepository.findByUserName("irfan") == null) {
			User user = new User("", passwordEncoder.encode("irfan123"), Arrays.asList("ADMIN"));
			this.userRepository.save(user);
		}
	}*/
	
	/*@Bean
	  @Autowired
	  public TokenStoreUserApprovalHandler userApprovalHandler(TokenStore tokenStore) {
	    TokenStoreUserApprovalHandler handler = new TokenStoreUserApprovalHandler();
	    handler.setTokenStore(tokenStore);
	    handler.setRequestFactory(new DefaultOAuth2RequestFactory(clientDetailsService));
	    handler.setClientDetailsService(clientDetailsService);
	    return handler;
	  }*/

	 @Bean
     public AuthenticationKeyGenerator authenticationKeyGenerator() {
         return new DefaultAuthenticationKeyGenerator();
     }
	 
	 
	/*
	 * @Bean
	 * 
	 * @Autowired public ApprovalStore approvalStore(TokenStore tokenStore) throws
	 * Exception { TokenApprovalStore store = new TokenApprovalStore();
	 * store.setTokenStore(tokenStore); return store; }
	 */
}
