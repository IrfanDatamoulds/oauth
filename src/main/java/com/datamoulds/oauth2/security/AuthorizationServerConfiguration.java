package com.datamoulds.oauth2.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;

import com.datamoulds.oauth2.service.TokenStoreService;

@SuppressWarnings("deprecation")
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	 @Autowired
	private TokenStoreService tokenStore;

	@Override
	public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
		security.allowFormAuthenticationForClients();
	}

	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients.inMemory()
				.withClient("irfan-client")
				.secret(passwordEncoder.encode("secret"))
				.authorizedGrantTypes("password", "client_credentials", "refresh_token","authorization_code")
				.scopes("all")
				.accessTokenValiditySeconds(30)
				.refreshTokenValiditySeconds(90);
	}

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		  endpoints.tokenStore(tokenStore)
		  .authenticationManager(authenticationManager);
		 
		
	}

	/*@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		  endpoints.tokenStore(tokenStore).userApprovalHandler(userApprovalHandler)
		  .authenticationManager(authenticationManager)
		  .userDetailsService(userDetailsService);
		 
		
	}*/
	

}
